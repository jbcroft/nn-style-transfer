# nn-style-transfer

An implementation of neural-network driven art style transfer. Based on this article: https://medium.com/tensorflow/neural-style-transfer-creating-art-with-deep-learning-using-tf-keras-and-eager-execution-7d541ac31398